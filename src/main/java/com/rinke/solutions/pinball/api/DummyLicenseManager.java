package com.rinke.solutions.pinball.api;

import com.rinke.solutions.pinball.api.LicenseException;
import com.rinke.solutions.pinball.api.LicenseManager;

public class DummyLicenseManager implements LicenseManager {

	@Override
	public VerifyResult verify(String filename) throws Exception {
		return new VerifyResult(false, 0);
	}

	@Override
	public String getLicenseFile() {
		return null;
	}

	@Override
	public VerifyResult getLicense() {
		return new VerifyResult(false, 0);
	}

	@Override
	public void requireOneOf(Capability... cap) {
		throw new LicenseException("no license at all");

	}

	@Override
	public void load() {
	}

	@Override
	public void save(String filename) {
	}

}
