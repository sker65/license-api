package com.rinke.solutions.pinball.api;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

import com.rinke.solutions.pinball.model.FrameSeq;
import com.rinke.solutions.pinball.model.PalMapping;
import com.rinke.solutions.pinball.model.Project;

public interface BinaryExporter {

	public abstract void writeTo(DataOutputStream os, PalMapping palMap)
			throws IOException;

	public abstract void writeTo(DataOutputStream os, FrameSeq fs)
			throws IOException;

	public abstract Map<String, Integer> writeFrameSeqTo(DataOutputStream os,
			Project pro) throws IOException;

	public abstract void writeTo(DataOutputStream os, Map<String, Integer> res,
			Project p) throws IOException;

}