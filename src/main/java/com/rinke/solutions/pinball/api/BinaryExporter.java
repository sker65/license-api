package com.rinke.solutions.pinball.api;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import com.rinke.solutions.pinball.model.FrameSeq;
import com.rinke.solutions.pinball.model.PalMapping;
import com.rinke.solutions.pinball.model.Project;

public interface BinaryExporter {

	public abstract void writeTo(DataOutputStream os, PalMapping palMap)
			throws IOException;

	public abstract void writeTo(DataOutputStream os, FrameSeq fs, int version)
			throws IOException;

	public abstract Map<String, Integer> writeFrameSeqTo(DataOutputStream os,
			Map <String,FrameSeq> frameSeqMap, int version) throws IOException;

	public abstract void writeTo(DataOutputStream os, Map<String, Integer> res,
			Project p) throws IOException;
	
	public abstract OutputStream buildStream(OutputStream in, String uid) throws RuntimeException;

}