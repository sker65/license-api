package com.rinke.solutions.pinball.api;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class LicenseManagerFactory {
	
	public static LicenseManager getInstance() {
		try {
			Class<?> clz = Class.forName("com.rinke.solutions.pinball.license.LicenseManagerImpl");
			Method method = clz.getMethod("getInstance");
			return (LicenseManager) method.invoke(null);
		} catch( ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException cnfe) {
			return new DummyLicenseManager();
		}
	}

}
