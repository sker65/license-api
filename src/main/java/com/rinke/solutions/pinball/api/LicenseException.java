package com.rinke.solutions.pinball.api;

public class LicenseException extends RuntimeException {

	private static final long serialVersionUID = 3156303440267990311L;

	public LicenseException(String message, Throwable cause) {
		super(message, cause);
	}

	public LicenseException(String message) {
		super(message);
	}

}
