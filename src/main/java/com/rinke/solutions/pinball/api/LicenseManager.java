package com.rinke.solutions.pinball.api;

import java.util.ArrayList;
import java.util.List;

public interface LicenseManager {

	public static enum Capability  {
		GODMD(16), 	  		//Bit 0 (byte 3) - goDMD
		VPIN(24), 	  		//Bit 0 - virtual Pin
		REALPIN(25),  		//Bit 1 - real Pin
		SMARTDMD(26), 		//Bit 2 - SmartDMD
		CRC32(27), 	  		//Bit 3 - CRC32
		REPLACE_FRAMES(28),
		XXL_DISPLAY(29),
		UNDEF(31),
		; //Bit 4 - Replacement
		//Bit 5 - unused
		public int bit;
		Capability(int bit) {
			this.bit = bit;
		}
	}
	
	public static class VerifyResult {

		public VerifyResult(boolean result, long c) {
			this.valid = result;
			this.capabilitiesAsLong = c;
		}

		public boolean valid;
		public long capabilitiesAsLong;
		public List<Capability> capabilities = new ArrayList<>();

		@Override
		public String toString() {
			return "VerifyResult [valid=" + valid + ", capabilities="
					+ capabilities + "]";
		}
	}

	public VerifyResult verify(String filename) throws Exception;
	
	public String getLicenseFile();

	public VerifyResult getLicense();

	public void requireOneOf(Capability ... cap);

	public void load();
	
	public void save(String filename);

}