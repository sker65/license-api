package com.rinke.solutions.pinball.api;

import java.lang.reflect.Method;

public class BinaryExporterFactory {
	
	public static BinaryExporter getInstance() {
		try {
			Class<?> clz = Class.forName("com.rinke.solutions.pinball.license.BinaryExporterImpl");
			Method method = clz.getMethod("getInstance");
			return (BinaryExporter) method.invoke(null);
		} catch( Exception e) {
			
		}
		return null;
	}

}
