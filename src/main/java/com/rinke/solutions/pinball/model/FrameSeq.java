package com.rinke.solutions.pinball.model;

import java.util.ArrayList;
import java.util.List;

public class FrameSeq implements Model {

    public List<Frame> frames;
    public List<Mask> masks;
    public String name;
    public int mask;		// which planes to write / to use
    public boolean reorderMask;
	
    public FrameSeq(List<Frame> frames, String name) {
		super();
		this.frames = frames;
		this.name = name;
		this.mask = 0x7FFFFF;
	}

	public FrameSeq(String name) {
		this(new ArrayList<Frame>(), name );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


	public int getMask() {
		return mask;
	}

	public void setMask(int mask) {
		this.mask = mask;
	}

	@Override
	public String toString() {
		return "FrameSeq [no of frames=" + frames.size() + ", name=" + name + ", mask=" + mask + "]";
	}

}
