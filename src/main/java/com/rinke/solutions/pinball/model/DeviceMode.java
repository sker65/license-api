package com.rinke.solutions.pinball.model;

public enum DeviceMode {
    PinMame, WPC, Stern, WhiteStar, Spike, DataEast, Gottlieb1, Gottlieb2, Gottlieb3, Capcom, AlvinG, Spooky, DE128x16, Inder, Sleic, HomePin 
}
