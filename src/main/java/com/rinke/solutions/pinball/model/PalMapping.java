package com.rinke.solutions.pinball.model;

import java.util.Arrays;
/**
 * a pal mapping to model switching of palettes
 * 
 * @author stefanri
 */
public class PalMapping implements Model {
	@Deprecated
	/** new version uses crc32 instead. */
	public byte digest[]; 
    
	public int palIndex;			// index of palette to switch to
    public int durationInMillis;
    
    @Deprecated
    public int durationInFrames;
    
    public int hashIndex; // which hash (from which frame)
    public String name;
    // just for backward comp
    @Deprecated
    public int animatisonIndex;
    public String animationName;	// name of ani to replace / add
    public int frameIndex;
    public String frameSeqName;
    
    // new in pro version
    public byte crc32[] = new byte[4];		// CRC32 checksum for frame match
    public static enum SwitchMode { PALETTE, REPLACE, ADD, EVENT };
    public SwitchMode switchMode;
    public boolean withMask;
    public int maskNumber;			// which mask was used

	public PalMapping(int palIndex, String name) {
		this.palIndex = palIndex;
		this.name = name;
	}

    public void setDigest(byte[] digest) {
    	for( int i = 0; i<4; i++ ) {
    		crc32[i] = digest[i];
    	}
    }

	@Override
	public String toString() {
		return String.format("PalMapping [palIndex=%s, durationInMillis=%s, name=%s, crc32=%s, switchMode=%s]", palIndex, durationInMillis, name,
				Arrays.toString(crc32), switchMode);
	}

    
}
