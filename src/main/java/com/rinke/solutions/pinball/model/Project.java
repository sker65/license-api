package com.rinke.solutions.pinball.model;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Project implements Model {
	private static final int NUMBER_OF_MASKS = 10;
	public byte version;
	public List<String> inputFiles;
	protected List<Palette> palettes;
	public HashMap<Integer, Palette> paletteMap;
	public HashMap<String, String> recordingNameMap;
	public List<PalMapping> palMappings;
	public List<Scene> scenes;
	public Map<String, Set<Bookmark>> bookmarksMap;
	public String name;
	public byte[] mask; // optional mask of 512 byte, that is used to mask out
						// dynamic plane elements
	// before checksum is calulated
	public boolean dirty;
	public List<Mask> masks;
	public int planeSize = 512;
	public int width;
	public int height;
	public int srcWidth;
	public int srcHeight;
	public int noOfPlanesWhenCutting;

	public Project() {
		version = 1;
		palettes = new ArrayList<Palette>();
		paletteMap = new HashMap<Integer, Palette>();
		recordingNameMap = new HashMap<String, String>();
		populatePaletteToMap(Palette.getDefaultPalettes());
		palMappings = new ArrayList<>();
		scenes = new ArrayList<>();
		inputFiles = new ArrayList<>();
		bookmarksMap = new HashMap<String, Set<Bookmark>>();
		// create default mask
		mask = new byte[planeSize];
		masks = new ArrayList<>();
		noOfPlanesWhenCutting = 4;
		resetMask();
	}

	public void setDimension(int w, int h) {
		if (w != width || h != height) { // only if one changes
			width = w;
			height = h;
			planeSize = w * h / 8;
			resetMask();
		}
	}

	public void setSrcDimension(int w, int h ) {
		if( w!=srcWidth || h!=srcHeight ) { // only if one changes
			srcWidth = w;
			srcHeight = h;
			resetMask();
		}
	}
	
	private void resetMask() {
		for (int i = 0; i < mask.length; i++) {
			mask[i] = (byte) 0xFF;
		}
		masks.clear();
		for (int i = 0; i < NUMBER_OF_MASKS; i++) {
			masks.add(new Mask(planeSize));
		}
	}

	public void clear() {
		palettes.clear();
		paletteMap.clear();
		populatePaletteToMap(Palette.getDefaultPalettes());
		inputFiles.clear();
		dirty = false;
		palMappings.clear();
		scenes.clear();
		masks.clear();
		resetMask();
	}

	@Override
	public String toString() {
		return "Project [version=" + version + ", inputFiles=" + inputFiles + ", palettes=" + palettes + ", palMappings=" + palMappings + "]";
	}

	public void writeTo(DataOutputStream os) throws IOException {
		throw new RuntimeException("use writeTo(DataOutputStream os, Map<String,Integer> res)");
	}

	public List<Palette> getPalettes() {
		return palettes;
	}

	public void setPalettes(List<Palette> palettes) {
		this.palettes = palettes;
	}

	public void populatePaletteToMap() {
		populatePaletteToMap(palettes);
		palettes.clear();
	}

	public void populatePaletteToMap(List<Palette> palettes) {
		for (Palette p : palettes) {
			paletteMap.put(p.index, p);
		}
	}

}
