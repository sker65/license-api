package com.rinke.solutions.pinball.model;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Project implements Model {
	private static final int NUMBER_OF_MASKS = 10;
	public byte version;
	public List<String> inputFiles;
	public List<Palette> palettes;
	public List<PalMapping> palMappings;
	public List<Scene> scenes;
	public Map<String,FrameSeq> frameSeqMap;
	public String name;
    public byte[] mask;		// optional mask of 512 byte, that is used to mask out dynamic plane elements
	// before checksum is calulated
 	public boolean dirty;
    public List<Mask> masks;
	
	public Project() {
        version = 1;
        palettes = new ArrayList<Palette>();
        palettes.addAll(Palette.getDefaultPalettes());
        palMappings = new ArrayList<>();
        scenes = new ArrayList<>();
        inputFiles=new ArrayList<>();
        frameSeqMap = new HashMap<String, FrameSeq>();
        // create default mask
        mask = new byte[512];
        masks = new ArrayList<>();
        resetMask();
    }

	private void resetMask() {
		for (int i = 0; i < mask.length; i++) {
			mask[i] = (byte)0xFF;
		}
		for( int i = 0; i < NUMBER_OF_MASKS; i++) {
			masks.add( new Mask() );
		}
	}
	
	public void clear() {
	    palettes.clear();
        palettes.addAll(Palette.getDefaultPalettes());
	    frameSeqMap.clear();
	    inputFiles.clear();
	    dirty = false;
	    palMappings.clear();
	    scenes.clear();
	    masks.clear();
	    resetMask();
	}

    @Override
	public String toString() {
		return "Project [version=" + version + ", inputFiles=" + inputFiles
				+ ", palettes=" + palettes + ", palMappings=" + palMappings
				+ "]";
	}
    
	public void writeTo(DataOutputStream os) throws IOException {
		throw new RuntimeException("use writeTo(DataOutputStream os, Map<String,Integer> res)");
	}

}
