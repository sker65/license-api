package com.rinke.solutions.pinball.model;

public class FrameLink {
	public String recordingName;
	public int frame;
	
	public FrameLink(String recordingName, int frame) {
		super();
		this.recordingName = recordingName;
		this.frame = frame;
	}
	
	public FrameLink(FrameLink src) {
		this.frame = src.frame;
		this.recordingName = src.recordingName;
	}

	@Override
	public String toString() {
		return String.format("FrameLink [recordingName=%s, frame=%s]", recordingName, frame);
	}
}
