package com.rinke.solutions.pinball.model;

import java.util.SortedSet;

public class Bookmark implements Comparable<Bookmark>{
	public String name;
	public int pos;
	
	public Bookmark(String name, int pos) {
		super();
		this.name = name;
		this.pos = pos;
	}
	
	@Override
	public String toString() {
		return String.format("Bookmark [name=%s, pos=%s]", name, pos);
	}
	
	@Override
	public int compareTo(Bookmark other) {
		return this.pos-other.pos;
	}

	public String getName() {
		return name;
	}

	public String getLabel() {
		return pos + " - " +name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + pos;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bookmark other = (Bookmark) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pos != other.pos)
			return false;
		return true;
	}
}
