package com.rinke.solutions.pinball.model;

import java.util.Arrays;

public class Mask {
	public byte[] data;
	public boolean locked;
	
	public Mask(int size) {
		super();
		data  = new byte[size];
		Arrays.fill(data, (byte)0xFF);
	}
	
	public Mask(Mask src) {
		this(src.data, src.locked);
	}
	
	public Mask(byte[] data, boolean locked) {
		super();
		this.data = Arrays.copyOf(data, data.length);
		this.locked = locked;
	}
	
	public void commit(byte[] data) {
		if(!this.locked ) {
			System.arraycopy(data, 0, this.data, 0, Math.min(data.length, this.data.length));
		} else {
			throw new RuntimeException("mask must not be locked");
		}
	}

	@Override
	public String toString() {
		return String.format("Mask [data=byte[%d], locked=%s]", data!=null?data.length:0, locked);
	}
}
