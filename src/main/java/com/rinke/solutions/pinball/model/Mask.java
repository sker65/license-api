package com.rinke.solutions.pinball.model;

public class Mask {
	public byte[] data = new byte[512];
	public boolean locked;
	public Mask() {
		super();
		for (int i = 0; i < data.length; i++) {
			data[i] = (byte)0xFF;
		}
	}
}
