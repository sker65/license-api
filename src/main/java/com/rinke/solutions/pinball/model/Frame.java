package com.rinke.solutions.pinball.model;

import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.CRC32;

import com.rinke.solutions.pinball.model.Model;

/**
 * frame model for dmd frames
 * @author stefanri
 */
public class Frame implements Model {
    
    public int delay;
    public int timecode;
    public List<Plane> planes = new ArrayList<>();

    public Frame(Frame src) {
    	this.delay = src.delay;
    	this.timecode = src.timecode;
    	for (Plane p : src.planes) {
			planes.add(new Plane(p.marker, p.plane));
		}
	}

	public Frame(byte[] plane1, byte[] plane2) {
        planes.add(new Plane((byte)0, plane1));
        planes.add(new Plane((byte)1, plane2));
    }

    public Frame( byte[] plane1, byte[] plane2, byte[] plane3, byte[] plane4) {
        planes.add(new Plane((byte)0, plane1));
        planes.add(new Plane((byte)1, plane2));
        planes.add(new Plane((byte)2, plane3));
        planes.add(new Plane((byte)3, plane4));
    }

    public Frame() {
	}
    
    public byte[] getPlaneBytes(int i) {
    	return planes.get(i).plane;
    }
    
    /**
     * return a list of hashes for each plane in this frame.
     * the hash consist of a simple 4 byte crc32 check sum 
     * @param mask 
     * @return
     */
	public List<byte[]> getHashes(byte[] mask) {
		List<byte[]> res = new ArrayList<>();
		for (Plane plane : planes) {
			CRC32 crc = new CRC32();
			if( mask != null ) {
				byte[] maskedPlane = new byte[plane.plane.length];
				for (int i = 0; i < mask.length; i++) {
					maskedPlane[i] = (byte) (plane.plane[i] & mask[i]);
				}
				crc.update(transform(maskedPlane), 0, maskedPlane.length);
			} else {
				crc.update(transform(plane.plane), 0, plane.plane.length);
			}
			byte[] b = new byte[4];
			long l = crc.getValue();
			for (int i = 3; i >= 0; i--) {
				b[i] = (byte) (l & 0xFF);
				l >>= 8;
			}
			res.add(b);
		}
		return res;

	}

	/**
	 * @see #transform(byte[], int, int)
	 * @param plane input byte array
	 * @return new array with swapped bits
	 */
    public static byte[] transform(byte[] plane) {
    	return transform(plane, 0, plane.length);
    }

    /**
     * transform a byte array in a way that in every byte the order of bits is swapped from LSB to MSB
     * @param data input array
     * @param offset offset in array
     * @param size size of array
     * @return new array with bits swapped
     */
	public static byte[] transform(byte[] data, int offset, int size) {
        byte[] res = new byte[size];
        for(int i = offset; i < size+offset; i++) {
            byte x = data[i];
            byte b = 0;
            for( int bit=0; bit<8; bit++){
                b<<=1;
                b|=( x &1);
                x>>=1;
            }
            res[i-offset]=b;
        }
        return res;
	}

    @Override
    public String toString() {
        return "Frame [delay=" + delay + ", timecode=" + timecode + ", planes="
                + planes + "]";
    }

    /**
     * binary output this frame to a given DataOutputStream
     * @param os the stream to write to
     */
	public void writeTo(DataOutputStream os, boolean transform) throws IOException {
		os.writeInt(delay);
		os.writeShort(planes.size());
		if( !planes.isEmpty() ) {
            os.writeShort(planes.get(0).plane.length);
		}
		for(Plane p : planes) {
			if( transform ) {
				os.write(Frame.transform(p.plane));
			} else {
				os.write(p.plane);
			}
		}
	}

	public boolean containsMask() {
		for(Plane p : planes) {
			if( p.marker > 16 ) return true;
		}
		return false;
	}

}