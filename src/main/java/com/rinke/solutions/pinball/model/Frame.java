package com.rinke.solutions.pinball.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.CRC32;

import static java.lang.Math.min;

/**
 * frame model for dmd frames
 * @author stefanri
 */
public class Frame implements Model {
    
    public int delay;
    public int timecode;
    public Mask mask;
    public List<Plane> planes = new ArrayList<>();
    public byte crc32[] = new byte[4];
    public FrameLink frameLink;
    public boolean keyFrame;
    
    public Frame(Frame src) {
    	this.delay = src.delay;
    	this.timecode = src.timecode;
    	this.setHash(src.crc32);
    	if( src.mask != null ) this.mask = new Mask(src.mask);
    	for (Plane p : src.planes) {
			planes.add(new Plane(p.marker, p.data));
		}
    	if( src.frameLink != null ) {
    		this.frameLink = new FrameLink(src.frameLink);
    	}
    	this.keyFrame = src.keyFrame;
	}

	public Frame(byte[] plane1, byte[] plane2) {
        planes.add(new Plane((byte)0, plane1));
        planes.add(new Plane((byte)1, plane2));
    }

    public Frame( byte[] plane1, byte[] plane2, byte[] plane3, byte[] plane4) {
        planes.add(new Plane((byte)0, plane1));
        planes.add(new Plane((byte)1, plane2));
        planes.add(new Plane((byte)2, plane3));
        planes.add(new Plane((byte)3, plane4));
    }

    public Frame(int bitPerChannel, int w, int h) {
    	int noFrames = bitPerChannel*3;
		for( int j = 0; j < noFrames ; j++) {
			this.planes.add(new Plane((byte)j, new byte[w*h/8]));
		}
	}
    
    public Frame() {
	}
    
    public byte[] getPlane(int i) {
    	return planes.get(i).data;
    }
    
    public void copyToWithMask( Frame dest, int planeMask ) {
    	if( (planeMask&1) != 0 && this.mask != null) {
    		dest.mask = new Mask(mask);
    	}
    	planeMask >>= 1;
    	// ensure dest has at least as many planes than source (this)
    	while( dest.planes.size() < this.planes.size()) {
    		dest.planes.add( new Plane((byte)dest.planes.size(), new byte[this.planes.get(0).data.length]));
    	}
		for( int i = 0; i < this.planes.size(); i++) {
			if( (planeMask&1) != 0) {
				int size = this.planes.get(i).data.length;
				System.arraycopy( this.planes.get(i).data, 0, dest.planes.get(i).data, 0, size);
			}
			planeMask >>= 1;
		}
		if( this.frameLink != null ) {
			dest.frameLink = new FrameLink(this.frameLink);
		}
    }
    
    /**
     * return a list of hashes for each plane in this frame.
     * the hash consist of a simple 4 byte crc32 check sum 
     * @param mask if null, use mask plane in frame
     * @return
     */
	public List<byte[]> getHashes() {
		List<byte[]> res = new ArrayList<>();
		int j = 0;
		byte[] maskData = null;
		if( hasMask() ) {
			maskData = mask.data;
		}
		for(; j < planes.size(); j++) {
			Plane plane = planes.get(j);
			CRC32 crc = new CRC32();
			if( maskData != null ) {
				byte[] maskedPlane = new byte[plane.data.length];
				for (int i = 0; i < min(maskData.length,plane.data.length); i++) {
					maskedPlane[i] = (byte) (plane.data[i] & maskData[i]);
				}
				crc.update(transform(maskedPlane), 0, maskedPlane.length);
			} else {
				crc.update(transform(plane.data), 0, plane.data.length);
			}
			byte[] b = new byte[4];
			long l = crc.getValue();
			for (int i = 3; i >= 0; i--) {
				b[i] = (byte) (l & 0xFF);
				l >>= 8;
			}
			res.add(b);
		}
		return res;
	}

	/**
	 * @see #transform(byte[], int, int)
	 * @param plane input byte array
	 * @return new array with swapped bits
	 */
    public static byte[] transform(byte[] plane) {
    	return transform(plane, 0, plane.length);
    }

    /**
     * transform a byte array in a way that in every byte the order of bits is swapped from LSB to MSB
     * @param data input array
     * @param offset offset in array
     * @param size size of array
     * @return new array with bits swapped
     */
	public static byte[] transform(byte[] data, int offset, int size) {
        byte[] res = new byte[size];
        for(int i = offset; i < size+offset; i++) {
            byte x = data[i];
            byte b = 0;
            for( int bit=0; bit<8; bit++){
                b<<=1;
                b|=( x &1);
                x>>=1;
            }
            res[i-offset]=b;
        }
        return res;
	}

    /**
     * binary output this frame to a given DataOutputStream
     * @param os the stream to write to
     */
	public void writeTo(DataOutputStream os, boolean transform, int version) throws IOException {
		os.writeInt(delay);
		int noOfPlanes = planes.size();
		if( hasMask() && version > 1) noOfPlanes++;
		os.writeShort(noOfPlanes);
		if( !planes.isEmpty() ) {
			// length of plane (all planes will be off the same length)
            os.writeShort(planes.get(0).data.length);
		}
		if( version > 1) {
			os.write(crc32);
			//System.out.println("writing hash " + formatHash(crc32)  );
		}
		if( hasMask() && version > 1) {
			writePlane(os, transform, version, mask.data, 1);
		}
		for(Plane p : planes) {
			writePlane(os, transform, version, p.data, 0);
		}
		if( version >= 7 ) {
			if( this.frameLink != null ) {
				os.writeByte(1);
				os.writeUTF(this.frameLink.recordingName);
				os.writeInt(this.frameLink.frame);
			} else {
				os.writeByte(0);
			}
		}
		if( version >= 8 ) {
			os.writeBoolean(this.keyFrame);
		}
	}

	private String formatHash(byte[] crc32) {
		StringBuilder sb = new StringBuilder();
		for( int i = 0; i < crc32.length; i++ ) sb.append(String.format("%02X", crc32[i]));
		return sb.toString();
	}

	private void writePlane(DataOutputStream os, boolean transform, int version, byte[] planeData, int mark) throws IOException {
		if( transform ) {
			if( version > 1) os.write(mark);
			os.write(Frame.transform(planeData));
		} else {
			if( version > 1) os.write(mark);
			os.write(planeData);
		}
	}
	
	public static Frame readFrom( DataInputStream is, boolean transform, int version ) throws IOException {
		Frame frame = new Frame();
		frame.delay = is.readInt();
		short planes = is.readShort();
		short planeSize = is.readShort();
		if( version > 1) {
			byte[] crc32 = new byte[4];
			is.read(crc32);
			frame.setHash(crc32);
		}
		int planeNo = 0;
		for(int i = 0; i < planes; i++) {
			byte[] data = new byte[planeSize];
			boolean isMask = false;
			if( transform ) {
				if( version > 1) isMask = is.read() == 1;
				is.read(data);
				data = Frame.transform(data);
			} else {
				if( version > 1) isMask = is.read() == 1;
				is.read(data);
			}
			Plane plane = new Plane((byte) (isMask?Plane.xMASK:planeNo), data);
			if( isMask ) {
				frame.mask = new Mask(plane.data, false);
			} else {
				frame.planes.add(plane);
				planeNo++;
			}
		}
		if( version >= 7 ) {
			byte hasLink = is.readByte();
			if( hasLink == 1) {
				frame.frameLink = new FrameLink( is.readUTF(), is.readInt() );
			}
		}
		if( version >= 8 ) {
			frame.keyFrame = is.readBoolean();
		}
		return frame;
	}
	
	public void setMask(Mask mask) {
		this.mask = mask;
	}

	public boolean hasMask() {
		return mask!=null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planes == null) ? 0 : planes.hashCode());
		result = prime * result + timecode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frame other = (Frame) obj;
		if (planes == null) {
			if (other.planes != null)
				return false;
		} else if (!planes.equals(other.planes))
			return false;
		if (timecode != other.timecode)
			return false;
		return true;
	}

	public void setHash(byte[] hash) {
		if( hash!=null) System.arraycopy(hash, 0, crc32, 0, crc32.length);
	}

	@Override
	public String toString() {
		return String.format("Frame [delay=%s, timecode=%s, mask=%s, planes=%s, crc32=%s, frameLink=%s]", delay, timecode, mask, planes, Arrays.toString(crc32),
				frameLink);
	}

}