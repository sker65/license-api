package com.rinke.solutions.pinball.model;

import java.util.Arrays;


public class Plane {
	public final static byte xMASK = (byte)'m';
    public byte marker;
    public byte[] data;
    
    public Plane(Plane src) {
    	super();
    	this.marker = src.marker;
    	this.data = Arrays.copyOf(src.data, src.data.length);
    }
    
    public Plane(byte marker, byte[] plane) {
        super();
        this.marker = marker;
        this.data = Arrays.copyOf(plane, plane.length);
    }
    @Override
    public String toString() {
        return "Plane [marker=" + marker + ", data=byte[" + data.length + "]]";
    }
    
    public void setData( byte[] in) {
    	this.data = Arrays.copyOf(in, data.length);
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + marker;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plane other = (Plane) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		if (marker != other.marker)
			return false;
		return true;
	}
}
