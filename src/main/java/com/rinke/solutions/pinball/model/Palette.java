package com.rinke.solutions.pinball.model;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Palette implements Model {
	public String name;
	public int numberOfColors;
	public int index;
	public RGB[] colors;
	public PaletteType type;

	public Palette(RGB[] colors, int index, String name) {
		this(colors, index, name, PaletteType.NORMAL);
	}

	public Palette(RGB[] colors, int index, String name, PaletteType type) {
		this(colors);
		this.index = index;
		this.name = name;
		this.type = type;
	}
	
	public String getLabel() {
		return index + " - " + name;
	}

	public Palette(RGB[] colors) {
		setColors(colors);
	}
	
	public boolean sameColors(RGB[] colors) {
		return Arrays.hashCode(colors) == Arrays.hashCode(this.colors);
	}

	public void writeTo(DataOutputStream os) throws IOException {
		os.writeShort(index);
		os.writeShort(numberOfColors);
		os.writeByte(type.ordinal());
		for (int i = 0; i < colors.length; i++) {
			os.writeByte(colors[i].red);
			os.writeByte(colors[i].green);
			os.writeByte(colors[i].blue);
		}
	}

	@Override
	public String toString() {
		return "Palette [name=" + name + ", numberOfColors=" + numberOfColors
				+ ", index=" + index 
				+ ", type=" + type + ", colors=" + Arrays.toString(colors) + "]";
	}

	public static List<Palette> getDefaultPalettes(int size) {
		List<Palette> palettes = new ArrayList<>();
		RGB rgb[] = new RGB[size];
		int palIndex = 0;
		int i = 0;
		if (size == 16){
			while( i < palColors16.length) {
				for( int j = 0; j<16; j++)
					rgb[j] = new RGB(palColors16[i++],palColors16[i++],palColors16[i++]);
				palettes.add(new Palette(rgb, palIndex, "pal"+palIndex));
				palIndex++;
			}
		} else if (size == 32){
			while( i < palColors32.length) {
				for( int j = 0; j<32; j++)
					rgb[j] = new RGB(palColors32[i++],palColors32[i++],palColors32[i++]);
				palettes.add(new Palette(rgb, palIndex, "pal"+palIndex));
				palIndex++;
			}
		} else {
			while( i < palColors64.length) {
				for( int j = 0; j<64; j++)
					rgb[j] = new RGB(palColors64[i++],palColors64[i++],palColors64[i++]);
				palettes.add(new Palette(rgb, palIndex, "pal"+palIndex));
				palIndex++;
			}
		}
		palettes.get(0).type = PaletteType.DEFAULT;
		return palettes;
	}

	public static List<Palette> getDefaultPalettes() {
		return getDefaultPalettes(16);
	}
	
	public static List<Palette> previewPalettes() {
		List<Palette> previewPalettes = new ArrayList<>();
	    RGB[] rgbPlane0 = { 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		};
		previewPalettes.add( new Palette(rgbPlane0, 0, "rgbPlane0", PaletteType.NORMAL) );
	    RGB[] rgbPlane1 = { 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		};
		previewPalettes.add( new Palette(rgbPlane1, 1, "rgbPlane1", PaletteType.NORMAL) );
	    RGB[] rgbPlane2 = { 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		};
		previewPalettes.add( new Palette(rgbPlane2) );
	    RGB[] rgbPlane3 = { 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    };
		previewPalettes.add( new Palette(rgbPlane3) );
	    RGB[] rgbPlane4 = { 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(0,0,0), 
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		new RGB(255,255,255),
	    		};
		previewPalettes.add( new Palette(rgbPlane4, 4, "plane415", PaletteType.NORMAL) );
	    RGB[] rgbPlaneAll = { 
	    		new RGB(0,0,0), 
	    		new RGB(0x11,0x11,0x11), 
	    		new RGB(0x22,0x22,0x22), 
	    		new RGB(0x33,0x33,0x33), 
	    		new RGB(0x44,0x44,0x44),
	    		new RGB(0x55,0x55,0x55), 
	    		new RGB(0x66,0x66,0x66), 
	    		new RGB(0x77,0x77,0x77), 
	    		new RGB(0x88,0x88,0x88), 
	    		new RGB(0x99,0x99,0x99), 
	    		new RGB(0xAA,0xAA,0xAA), 
	    		new RGB(0xBB,0xBB,0xBB),
	    		new RGB(0xCC,0xCC,0xCC), 
	    		new RGB(0xDD,0xDD,0xDD), 
	    		new RGB(0xEE,0xEE,0xEE), 
	    		new RGB(0xFF,0xFF,0xFF), 
	    		};
		previewPalettes.add( new Palette(rgbPlaneAll, 4, "planeAll", PaletteType.NORMAL) );
		return Collections.unmodifiableList(previewPalettes);
	}

	static short[] palColors16 = {
					0x00,0x00,0x00, 	// black
					0x77,0x00,0x00, 	// dark red
					//0x7e,0x6b,0xff,
					0xFF,0x00,0x00, 	// red
					//0xa4,0x15,0xc6,
					0xFF,0x00,0xFF, 	// purple
					//0xff,0xfa,0x6b,
					0x00,0x77,0x77, 	// teal
					0x00,0x77,0x00, 	// green
					0x00,0xFF,0x00, 	// bright green
					0x00,0xFF,0xFF, 	// turquoise
					0x00,0x00,0x77,		// dark blue
					0x77,0x00,0x77, 	// violet
					0x00,0x00,0xFF, 	// blue
					0x33,0x33,0x33, 	// gray 25%
					0x77,0x77,0x77,		// gray 50%
					0x77,0x77,0x00, 	// dark yellow
					0xFF,0xFF,0x00,  	// yellow
					0xFF,0xFF,0xFF,		// white
//	Palette redPalette = { 1,
					0x00,0x00,0x00,
					0x11,0x00,0x00,
					0x22,0x00,0x00,
					0x33,0x00,0x00,
					0x44,0x00,0x00,
					0x55,0x00,0x00,
					0x66,0x00,0x00,
					0x77,0x00,0x00,
					0x88,0x00,0x00,
					0x99,0x00,0x00,
					0xAA,0x00,0x00,
					0xBB,0x00,0x00,
					0xCC,0x00,0x00,
					0xDD,0x00,0x00,
					0xEE,0x00,0x00,
					0xFF,0x00,0x00,
//	Palette greenPalette = { 2,
					0x00,0x00,0x00,
					0x00,0x11,0x00,
					0x00,0x22,0x00,
					0x00,0x33,0x00,
					0x00,0x44,0x00,
					0x00,0x55,0x00,
					0x00,0x66,0x00,
					0x00,0x77,0x00,
					0x00,0x88,0x00,
					0x00,0x99,0x00,
					0x00,0xAA,0x00,
					0x00,0xBB,0x00,
					0x00,0xCC,0x00,
					0x00,0xDD,0x00,
					0x00,0xEE,0x00,
					0x00,0xFF,0x00,
//	Palette bluePalette = { 3,
					0x00,0x00,0x00,
					0x00,0x00,0x11,
					0x00,0x00,0x22,
					0x00,0x00,0x33,
					0x00,0x00,0x44,
					0x00,0x00,0x55,
					0x00,0x00,0x66,
					0x00,0x00,0x77,
					0x00,0x00,0x88,
					0x00,0x00,0x99,
					0x00,0x00,0xAA,
					0x00,0x00,0xBB,
					0x00,0x00,0xCC,
					0x00,0x00,0xDD,
					0x00,0x00,0xEE,
					0x00,0x00,0xFF,
//	Palette purplePalette = { 4,
					0x00,0x00,0x00,
					0x11,0x00,0x11,
					0x22,0x00,0x22,
					0x33,0x00,0x33,
					0x44,0x00,0x44,
					0x55,0x00,0x55,
					0x66,0x00,0x66,
					0x77,0x00,0x77,
					0x88,0x00,0x88,
					0x99,0x00,0x99,
					0xAA,0x00,0xAA,
					0xBB,0x00,0xBB,
					0xCC,0x00,0xCC,
					0xDD,0x00,0xDD,
					0xEE,0x00,0xEE,
					0xFF,0x00,0xFF,
//	Palette cyanPalette = { 5,
					0x00,0x00,0x00,
					0x00,0x11,0x11,
					0x00,0x22,0x22,
					0x00,0x33,0x33,
					0x00,0x44,0x44,
					0x00,0x55,0x55,
					0x00,0x66,0x66,
					0x00,0x77,0x77,
					0x00,0x88,0x88,
					0x00,0x99,0x99,
					0x00,0xAA,0xAA,
					0x00,0xBB,0xBB,
					0x00,0xCC,0xCC,
					0x00,0xDD,0xDD,
					0x00,0xEE,0xEE,
					0x00,0xFF,0xFF,
//	Palette yellowPalette = { 6,
					0x00,0x00,0x00,
					0x11,0x11,0x00,
					0x22,0x22,0x00,
					0x33,0x33,0x00,
					0x44,0x44,0x00,
					0x55,0x55,0x00,
					0x66,0x66,0x00,
					0x77,0x77,0x00,
					0x88,0x88,0x00,
					0x99,0x99,0x00,
					0xAA,0xAA,0x00,
					0xBB,0xBB,0x00,
					0xCC,0xCC,0x00,
					0xDD,0xDD,0x00,
					0xEE,0xEE,0x00,
					0xFF,0xFF,0x00,
//	Palette greyPalette = { 7,
					0x00,0x00,0x00,
					0x11,0x11,0x11,
					0x22,0x22,0x22,
					0x33,0x33,0x33,
					0x44,0x44,0x44,
					0x55,0x55,0x55,
					0x66,0x66,0x66,
					0x77,0x77,0x77,
					0x88,0x88,0x88,
					0x99,0x99,0x99,
					0xAA,0xAA,0xAA,
					0xBB,0xBB,0xBB,
					0xCC,0xCC,0xCC,
					0xDD,0xDD,0xDD,
					0xEE,0xEE,0xEE,
					0xFF,0xFF,0xFF,
					
//	Palette orangePalette = { 8,
					0x00,0x00,0x00,
					0x11,0x09,0x00,
					0x22,0x12,0x00,
					0x33,0x1C,0x00,
					0x44,0x25,0x00,
					0x55,0x2E,0x00,
					0x66,0x38,0x00,
					0x77,0x3B,0x00,
					0x88,0x4A,0x00,
					0x99,0x54,0x00,
					0xAA,0x5D,0x00,
					0xBB,0x66,0x00,
					0xCC,0x70,0x00,
					0xDD,0x79,0x00,
					0xEE,0x82,0x00,
					0xFF,0x8C,0x00,
			};

static short[] palColors32 = {
					0x00,0x00,0x00, 	// black
					0x33,0x00,0x00, 	// red
					0x77,0x00,0x00,
					0xFF,0x00,0x00,
					0x00,0x33,0x00, 	//green
					0x00,0x77,0x00,
					0x00,0xFF,0x00,
					0x00,0x00,0x33,		//blue
					0x00,0x00,0x77,
					0x00,0x00,0xFF,
					0x33,0x33,0x00, 	//yellow
					0x77,0x77,0x00, 	
					0xFF,0xFF,0x00, 	
					0x33,0x33,0x33,		//white
					0x77,0x77,0x77,
					0xFF,0xFF,0xFF,
					0x00,0x00,0x00,
					0x33,0x00,0x33,		//voilet
					0x77,0x00,0x77, 	
					0xFF,0x00,0xFF,  	
					0x00,0x33,0x22,		//
					0x00,0x77,0x33,
					0x00,0xFF,0x77,
					0x00,0x22,0x33,		//
					0x00,0x33,0x77,
					0x00,0x77,0xFF,
					0x00,0x33,0x33,		//cyan
					0x00,0x77,0x77,
					0x00,0xFF,0xFF,
					0x33,0x1C,0x00,		//orange
					0x77,0x3B,0x00,
					0xFF,0x8C,0x00,
			};

static short[] palColors64 = {
					0x00, 0x00, 0x00,
					0x00, 0x00, 0xAA,
					0x00, 0xAA, 0x00,
					0x00, 0xAA, 0xAA,
					0xAA, 0x00, 0x00,
					0xAA, 0x00, 0xAA,
					0xAA, 0xAA, 0x00,
					0xAA, 0xAA, 0xAA,
					0x00, 0x00, 0x55,
					0x00, 0x00, 0xFF,
					0x00, 0xAA, 0x55,
					0x00, 0xAA, 0xFF,
					0xAA, 0x00, 0x55,
					0xAA, 0x00, 0xFF,
					0xAA, 0xAA, 0x55,
					0xAA, 0xAA, 0xFF,
					0x00, 0x55, 0x00,
					0x00, 0x55, 0xAA,
					0x00, 0xFF, 0x00,
					0x00, 0xFF, 0xAA,
					0xAA, 0x55, 0x00,
					0xAA, 0x55, 0xAA,
					0xAA, 0xFF, 0x00,
					0xAA, 0xFF, 0xAA,
					0x00, 0x55, 0x55,
					0x00, 0x55, 0xFF,
					0x00, 0xFF, 0x55,
					0x00, 0xFF, 0xFF,
					0xAA, 0x55, 0x55,
					0xAA, 0x55, 0xFF,
					0xAA, 0xFF, 0x55,
					0xAA, 0xFF, 0xFF,
					0x55, 0x00, 0x00,
					0x55, 0x00, 0xAA,
					0x55, 0xAA, 0x00,
					0x55, 0xAA, 0xAA,
					0xFF, 0x00, 0x00,
					0xFF, 0x00, 0xAA,
					0xFF, 0xAA, 0x00,
					0xFF, 0xAA, 0xAA,
					0x55, 0x00, 0x55,
					0x55, 0x00, 0xFF,
					0x55, 0xAA, 0x55,
					0x55, 0xAA, 0xFF,
					0xFF, 0x00, 0x55,
					0xFF, 0x00, 0xFF,
					0xFF, 0xAA, 0x55,
					0xFF, 0xAA, 0xFF,
					0x55, 0x55, 0x00,
					0x55, 0x55, 0xAA,
					0x55, 0xFF, 0x00,
					0x55, 0xFF, 0xAA,
					0xFF, 0x55, 0x00,
					0xFF, 0x55, 0xAA,
					0xFF, 0xFF, 0x00,
					0xFF, 0xFF, 0xAA,
					0x55, 0x55, 0x55,
					0x55, 0x55, 0xFF,
					0x55, 0xFF, 0x55,
					0x55, 0xFF, 0xFF,
					0xFF, 0x55, 0x55,
					0xFF, 0x55, 0xFF,
					0xFF, 0xFF, 0x55,
					0xFF, 0xFF, 0xFF,
			};


	public void setColors(RGB[] colors) {
		this.colors = new RGB[colors.length];
		this.numberOfColors = colors.length;
		this.colors = Arrays.copyOf(colors, colors.length);
	}

	public static RGB[] defaultColors(int size) {
		RGB rgb[] = new RGB[size];
		int i = 0;
		if (size == 16) {
			while( i < palColors16.length) {
				for( int j = 0; j<16; j++)
					rgb[j] = new RGB(palColors16[i++],palColors16[i++],palColors16[i++]);
			}
		} else if (size == 32) {
			while( i < palColors32.length) {
				for( int j = 0; j<32; j++)
					rgb[j] = new RGB(palColors32[i++],palColors32[i++],palColors32[i++]);
			}
		} else {
			while( i < palColors64.length) {
				for( int j = 0; j<64; j++)
					rgb[j] = new RGB(palColors64[i++],palColors64[i++],palColors64[i++]);
			}
		}
		return rgb;
	}

	public static RGB[] defaultColors() {
		return defaultColors(16);
	}
}
