package com.rinke.solutions.pinball.model;

public enum DefaultPalette {
    Vga, Red, Green, Blue, Pink, Cyan, Yellow, Grey, Orange
}
