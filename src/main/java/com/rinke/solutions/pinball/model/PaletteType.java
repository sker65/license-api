package com.rinke.solutions.pinball.model;

public enum PaletteType {
	NORMAL("normal"), DEFAULT("default");
	
	public final String label;

	private PaletteType(String label) {
		this.label = label;
	}
}
