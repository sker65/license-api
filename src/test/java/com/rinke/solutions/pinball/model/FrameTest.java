package com.rinke.solutions.pinball.model;

import static org.junit.Assert.assertArrayEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import org.junit.Test;


public class FrameTest {

	byte[] plane = new byte[512];
	
	@Test
	public void testWriteTo() throws Exception {
		Frame frame = new Frame(plane,plane);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);
		frame.writeTo(dos, false, 2);
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		DataInputStream dis = new DataInputStream(bis);
		Frame frame2 = Frame.readFrom(dis , false, 2);
		assertArrayEquals(frame.crc32, frame2.crc32);
		assertArrayEquals(frame.planes.get(0).data,frame2.planes.get(0).data);
	}
	
	@Test
	public void testCopyToWithMaske() throws Exception {
		byte[] plane0 = new byte[512];
		byte[] plane1 = new byte[512];
		Frame src = new Frame(plane0, plane1);
		Frame dest = new Frame(new byte[512], new byte[512]);
		src.copyToWithMask(dest, 0);
		src.mask = new Mask(512);
		src.copyToWithMask(dest, 0);
		src.copyToWithMask(dest, 0b0111);
		src.planes.add(new Plane((byte) 2, new byte[512]));
		src.copyToWithMask(dest, 0b0111);
	}



}
